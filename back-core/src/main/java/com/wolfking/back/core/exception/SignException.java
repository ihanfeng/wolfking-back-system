/**
 * 
 */
package com.wolfking.back.core.exception;

/**
 * 校验的登录的异常
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日下午4:50:16
 * @版权 归wolfking所有
 */
public class SignException extends RuntimeException {

	private static final long serialVersionUID = 2165753414466525246L;

	private String permission;

	public SignException() {
	}

	public SignException(String permission) {
		this.permission = permission;
	}

	/**
	 * @return permission
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * @param permission
	 *            要设置的 permission
	 */
	public void setPermission(String permission) {
		this.permission = permission;
	}

}
