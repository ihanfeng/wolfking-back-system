package com.wolfking.back.core.bean;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyTable;

/**
 * 角色是实体
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月13日 下午9:05:58
 * @copyright wolfking
 */
@MyTable("sys_role")
public class Role extends DataEntity {
	private static final long serialVersionUID = -6505598406354271488L;
	@MyColumn("office_id")
	private String officeId; // 归属机构
	@MyColumn
	private String name; // 角色名称
	@MyColumn
	private String enname; // 英文名称
	@MyColumn("role_type")
	private String roleType;// 权限类型
	@MyColumn("data_scope")
	private String dataScope;// 数据范围
	@MyColumn("is_sys")
	private String sysData; // 是否是系统数据
	@MyColumn
	private String useable; // 是否是可用

	private String menuIds; // 菜单ID的集合

	private String officeIds; // 组织结构ID的集合

	private String oldName; // 原角色名称
	private String oldEnname; // 原英文名称

	// 数据范围（1：所有数据；2：所在公司及以下数据；3：所在公司数据；4：所在部门及以下数据；5：所在部门数据；8：仅本人数据；9：按明细设置）
	public static final String DATA_SCOPE_ALL = "1";
	public static final String DATA_SCOPE_COMPANY_AND_CHILD = "2";
	public static final String DATA_SCOPE_COMPANY = "3";
	public static final String DATA_SCOPE_OFFICE_AND_CHILD = "4";
	public static final String DATA_SCOPE_OFFICE = "5";
	public static final String DATA_SCOPE_SELF = "8";
	public static final String DATA_SCOPE_CUSTOM = "9";

	public Role() {
		super();
	}

	/**
	 * @return officeId
	 */
	public String getOfficeId() {
		return officeId;
	}

	/**
	 * @param officeId
	 *            要设置的 officeId
	 */
	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return enname
	 */
	public String getEnname() {
		return enname;
	}

	/**
	 * @param enname
	 *            要设置的 enname
	 */
	public void setEnname(String enname) {
		this.enname = enname;
	}

	/**
	 * @return roleType
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * @param roleType
	 *            要设置的 roleType
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 * @return dataScope
	 */
	public String getDataScope() {
		return dataScope;
	}

	/**
	 * @param dataScope
	 *            要设置的 dataScope
	 */
	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}

	/**
	 * @return sysData
	 */
	public String getSysData() {
		return sysData;
	}

	/**
	 * @param sysData
	 *            要设置的 sysData
	 */
	public void setSysData(String sysData) {
		this.sysData = sysData;
	}

	/**
	 * @return useable
	 */
	public String getUseable() {
		return useable;
	}

	/**
	 * @param useable
	 *            要设置的 useable
	 */
	public void setUseable(String useable) {
		this.useable = useable;
	}

	/**
	 * @return oldName
	 */
	public String getOldName() {
		return oldName;
	}

	/**
	 * @param oldName
	 *            要设置的 oldName
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	/**
	 * @return oldEnname
	 */
	public String getOldEnname() {
		return oldEnname;
	}

	/**
	 * @param oldEnname
	 *            要设置的 oldEnname
	 */
	public void setOldEnname(String oldEnname) {
		this.oldEnname = oldEnname;
	}

	/**
	 * @return menuIds
	 */
	public String getMenuIds() {
		return menuIds;
	}

	/**
	 * @param menuIds
	 *            要设置的 menuIds
	 */
	public void setMenuIds(String menuIds) {
		this.menuIds = menuIds;
	}

	/**
	 * @return officeIds
	 */
	public String getOfficeIds() {
		return officeIds;
	}

	/**
	 * @param officeIds
	 *            要设置的 officeIds
	 */
	public void setOfficeIds(String officeIds) {
		this.officeIds = officeIds;
	}

}
