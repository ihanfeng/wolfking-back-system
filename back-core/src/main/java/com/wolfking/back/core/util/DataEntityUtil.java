package com.wolfking.back.core.util;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.wolfking.back.core.bean.DataEntity;
import com.wolfking.back.core.bean.User;

/**
 * dataEntity 添加数据
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月2日上午10:03:43
 * @版权 归wolfking所有
 */
public class DataEntityUtil {

	public static void assemblyDataEntity(DataEntity entity, User user) {
		if (entity != null && user != null) {
			if (StringUtils.isBlank(entity.getId())) {
				entity.setCreateDate(new Date());
				entity.setCreateBy(user.getId());
			}
			entity.setUpdateDate(new Date());
			entity.setUpdateBy(user.getId());
		}
	}
}
