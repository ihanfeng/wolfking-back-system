package com.wolfking.back.core.util;

import javax.inject.Singleton;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * spring的工具类
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日上午9:15:37
 * @版权 归wolfking所有
 */
@Lazy
@Singleton
@Component
public class WolfkingSpringUtil implements ApplicationContextAware {
	
	private static ApplicationContext ctx;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		WolfkingSpringUtil.ctx = applicationContext;
	}

	/**
	 * 根据名称获取Bean
	 * 
	 * @param name
	 *            Bean的名称
	 * 
	 * @return 获取到的Bean对象，类型为Object
	 */
	public static Object getBean(String name) {
		return ctx.getBean(name);
	}

	/**
	 * 根据类型获取Bean
	 * 
	 * @param clazz
	 *            Bean类型
	 * 
	 * @return 获取到的Bean对象
	 */
	public static <T> T getBean(Class<T> clazz) {
		return ctx.getBean(clazz);
	}

}
