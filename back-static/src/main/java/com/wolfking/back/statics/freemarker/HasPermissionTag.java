package com.wolfking.back.statics.freemarker;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import freemarker.core.Environment;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * freemarker的权限的标签
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月28日下午3:50:30
 * @版权 归wolfking所有
 */
@SuppressWarnings("rawtypes")
public class HasPermissionTag implements TemplateDirectiveModel {

	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		Set<String> pSet = new HashSet<>();
		Object object = env.getVariable("permissionCodes");
		SimpleSequence obj = (SimpleSequence) object;
		if (obj != null) {
			for (int i = 0; i < obj.size(); i++)
				pSet.add(String.valueOf(obj.get(i)));
			if (params.containsKey("code") && pSet.contains(params.get("code").toString()))
				body.render(env.getOut());
		}
	}

}
