package com.wolfking.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 服务网关
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月16日 下午9:14:01
 * @copyright wolfking
 */
@EnableZuulProxy
@SpringBootApplication
//@EnableRequestCorrelation
public class GateServer {
	public static void main(String args[]) {
		SpringApplication.run(GateServer.class, args);
	}
}
